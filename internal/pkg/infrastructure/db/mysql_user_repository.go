package db

import (
	"context"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
)

type MySQLUserRepository struct {
	db *sqlx.DB
}

func NewMySQLUserRepository(db *sqlx.DB) *MySQLUserRepository {
	if db == nil {
		panic("mysql db is nil")
	}

	return &MySQLUserRepository{
		db: db,
	}
}

func (r MySQLUserRepository) Save(ctx context.Context, u *user.User) (err error) {
	tx, err := r.db.Beginx()
	if err != nil {
		return fmt.Errorf("unable to start transactions: %w", err)
	}
	defer func() {
		err = finishTransaction(tx, err)
	}()

	stmt, err := tx.PrepareContext(ctx, "INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return fmt.Errorf("statement cannot be created: %w", err)
	}
	defer stmt.Close()

	// Using AutoIncrement id
	// If we need to check the result that can be done. Need to think
	result, err := stmt.ExecContext(ctx, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	if err != nil {
		return fmt.Errorf("statement cannot be executed: %w", err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		return fmt.Errorf("id cannot be retrieved: %w", err)
	}

	created := user.UnmarshalFromPersistenceUnit(int32(id), u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	u = &created

	return nil
}

func NewMysqlConnection(port int, host, username, password, dbName string) *sqlx.DB {
	dsn := fmt.Sprintf("%s:%s@%s:%d/%s", username, password, host, port, dbName)
	db, err := sqlx.Open("mysql", dsn)
	if err != nil {
		panic(fmt.Errorf("mysql db cannot be created: %w", err))
	}
	return db
}
