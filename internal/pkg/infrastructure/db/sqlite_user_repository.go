package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
)

type SQLiteUserRepository struct {
	db *sqlx.DB
}

func NewSQLiteUserRepository(db *sqlx.DB) *SQLiteUserRepository {
	if db == nil {
		panic("sqlite db is nil")
	}

	return &SQLiteUserRepository{
		db: db,
	}
}

func (r SQLiteUserRepository) Save(ctx context.Context, u *user.User) (err error) {
	tx, err := r.db.Beginx()
	if err != nil {
		return fmt.Errorf("unable to start transactions: %w", err)
	}
	defer func() {
		err = finishTransaction(tx, err)
	}()

	stmt, err := tx.PrepareContext(ctx, "INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return fmt.Errorf("statement cannot be created: %w", err)
	}
	defer stmt.Close()

	// Using AutoIncrement id
	// If we need to check the result that can be done. Need to think
	result, err := stmt.ExecContext(ctx, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	if err != nil {
		return fmt.Errorf("statement cannot be executed: %w", err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		return fmt.Errorf("id cannot be retrieved: %w", err)
	}

	created := user.UnmarshalFromPersistenceUnit(int32(id), u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	u = &created

	return nil
}

func NewSQLiteConnection(filename string) *sqlx.DB {
	db, err := sqlx.Open("sqlite3", filename)
	if err != nil {
		panic(fmt.Errorf("sqlite3 db cannot be created: %w", err))
	}

	return db
}
