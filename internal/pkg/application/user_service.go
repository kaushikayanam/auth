package application

import (
	"context"

	"nononsensecode.com/auth/internal/pkg/domain/model/user"
)

type UserService struct {
	userRepo user.Repository
}

func NewUserService(r user.Repository) *UserService {
	if r == nil {
		panic("user repository is nil")
	}

	return &UserService{
		userRepo: r,
	}
}

func (us UserService) RegisterUser(ctx context.Context, username, firstName, lastName, email, password string) (user.User, error) {
	u := user.New(username, firstName, lastName, email, password)
	if err := us.userRepo.Save(ctx, u); err != nil {
		return user.User{}, err
	}
	return u, nil
}
