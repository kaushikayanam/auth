package user

import "context"

type Repository interface {
	Save(context.Context, *User) error
}
