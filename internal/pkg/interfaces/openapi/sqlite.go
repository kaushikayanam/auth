package openapi

import (
	"nononsensecode.com/auth/internal/configs"
	"nononsensecode.com/auth/internal/pkg/application"
	"nononsensecode.com/auth/internal/pkg/infrastructure/db"
)

func NewSQLiteServer(cfg configs.Configurations) Server {
	dbConn := db.NewSQLiteConnection(cfg.Database.FileName)
	userRepo := db.NewSQLiteUserRepository(dbConn)
	userService := application.NewUserService(userRepo)
	return Server{
		userService: userService,
	}
}
