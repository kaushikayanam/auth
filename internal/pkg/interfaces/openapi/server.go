package openapi

import (
	"net/http"

	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
	"nononsensecode.com/auth/internal/pkg/application"
)

type Server struct {
	userService *application.UserService
}

func (s Server) RegisterUser(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		logrus.Errorf("user form cannot be parsed: %v", err)
		eDto := errorDto("auth", "invalid user form", http.StatusBadRequest)
		w.WriteHeader(*eDto.Status)
		render.Respond(w, r, eDto)
		return
	}

	uDto := new(UserDTO)
	if err := uDto.Bind(r); err != nil {
		logrus.Errorf("user details are incorrect: %v", err)
		eDto := errorDto("auth", "invalid user details", http.StatusBadRequest)
		w.WriteHeader(*eDto.Status)
		render.Respond(w, r, eDto)
		return
	}

	u, err := s.userService.RegisterUser(r.Context(), *uDto.Username, *uDto.FirstName,
		*uDto.LastName, *uDto.Email, *uDto.Password)
	if err != nil {
		logrus.Errorf("cannot save the user: %v", err)
		eDto := errorDto("auth", "cannot save the user", http.StatusInternalServerError)
		w.WriteHeader(*eDto.Status)
		render.Respond(w, r, eDto)
		return
	}

	uidDto := UserIdDTO{Id: u.ID()}
	render.Respond(w, r, uidDto)
}

func (u *UserDTO) Bind(r *http.Request) error {
	// Here we should add validations
	username := r.PostFormValue("username")
	firstName := r.PostFormValue("firstName")
	lastName := r.PostFormValue("lastName")
	email := r.PostFormValue("email")
	password := r.PostFormValue("password")

	u.Username = &username
	u.FirstName = &firstName
	u.LastName = &lastName
	u.Email = &email
	u.Password = &password

	return nil
}

func errorDto(comp, msg string, status int) ErrorDTO {
	return ErrorDTO{
		Component: &comp,
		Msg:       &msg,
		Status:    &status,
	}
}
