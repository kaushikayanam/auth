package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var gocliCmd = &cobra.Command{
	Use:   "gocli",
	Short: "command to start auth server, import and export db",
}

func init() {
	gocliCmd.AddCommand(apiCmd)
}

func Execute() {
	if err := gocliCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
