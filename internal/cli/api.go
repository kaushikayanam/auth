package cli

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"nononsensecode.com/auth/internal/configs"
	"nononsensecode.com/auth/internal/pkg/interfaces/openapi"
	"nononsensecode.com/auth/pkg/common/server"
)

var (
	cfgFile string
	cfg     *configs.Configurations
	apiCmd  = &cobra.Command{
		Use:   "api",
		Short: "start the api server according to configuration file",
		Run:   initServer,
	}
)

func init() {
	cobra.OnInitialize(initConfig)

	apiCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "configuration file in yaml format. Default is ./config.yaml")
	// if err := apiCmd.MarkFlagRequired("config"); err != nil {
	// 	fmt.Println(err)
	// 	os.Exit(2)
	// }
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in home directory with name ".cobra" (without extension).
		viper.AddConfigPath("./")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	cfg = new(configs.Configurations)
	if err := viper.Unmarshal(cfg); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}

func initServer(cmd *cobra.Command, args []string) {
	cfg.Init()
	oaServer := openapi.NewSQLiteServer(*cfg)
	server.RunHTTPServer(cfg.Infra.Server.HostWithPort(), func(router chi.Router) http.Handler {
		return openapi.HandlerFromMux(
			oaServer,
			router,
		)
	}, cors.Options{}, cfg.Infra.Server.ApiPrefix)
}
