package configs

import "log"

func (c *Configurations) Init() {
	if !c.UseCloudConfig {
		return
	}

	var infra InfraConfiguration
	var err error
	switch c.CloudVendor {
	case "aws":
		infra, err = c.initAwsConfig()
		if err != nil {
			log.Fatalf("error occurred while configuring using aws: %v", err)
		}
	}

	c.Infra = infra
}
