package configs

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

const (
	ErrAwsNil                 = "aws configuration is invalid"
	ErrAwsSessCreation        = "aws session cannot be created"
	ErrAwsCredRetrieval       = "aws credential cannot be retrieved"
	ErrAwsSecretsRetrieval    = "aws secret cannot be retrieved"
	ErrAwsSecretsStringDecode = "aws secret string cannot be decoded"
)

func (c *Configurations) initAwsConfig() (InfraConfiguration, error) {
	if c.AWS.IsNil() {
		return InfraConfiguration{}, errors.New(ErrAwsNil)
	}

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(c.AWS.Region),
		Credentials: credentials.NewSharedCredentials("", c.AWS.Profile),
		Endpoint:    aws.String(c.AWS.Endpoint),
	})
	if err != nil {
		return InfraConfiguration{}, fmt.Errorf("%s:%w", ErrAwsSessCreation, err)
	}

	_, err = sess.Config.Credentials.Get()
	if err != nil {
		return InfraConfiguration{}, fmt.Errorf("%s:%w", ErrAwsCredRetrieval, err)
	}

	sMgr := secretsmanager.New(sess)
	sInput := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(c.AWS.SecretName),
	}
	sOutput, err := sMgr.GetSecretValue(sInput)
	if err != nil {
		return InfraConfiguration{}, fmt.Errorf("%s:%w", ErrAwsSecretsRetrieval, err)
	}

	var secretString string
	if sOutput.SecretString == nil {
		sBytes := make([]byte, base64.StdEncoding.DecodedLen(len(sOutput.SecretBinary)))
		len, err := base64.StdEncoding.Decode(sBytes, sOutput.SecretBinary)
		if err != nil {
			return InfraConfiguration{}, fmt.Errorf("%s:%w", ErrAwsSecretsStringDecode, err)
		}
		secretString = string(sBytes[:len])
	} else {
		secretString = *sOutput.SecretString
	}

	var infra InfraConfiguration
	json.Unmarshal([]byte(secretString), &infra)

	return infra, nil
}
