package configs

import (
	"fmt"
	"log"
	"strings"
)

// Configurations
type Configurations struct {
	UseCloudConfig bool               `mapstructure:"useCloudConfig"`
	CloudVendor    string             `mapstructure:"cloudVendor"`
	AWS            AWSConfiguration   `mapstructure:"aws"`
	Infra          InfraConfiguration `mapstructure:"infra"`
}

type InfraConfiguration struct {
	Server   ServerConfigurations   `mapstructure:"server" json:"server"`
	Database DatabaseConfigurations `mapstructure:"db" json:"db"`
}

// ServerConfigurations
type ServerConfigurations struct {
	Host      string `mapstructure:"host" json:"host"`
	Port      int    `mapstructure:"port" json:"port"`
	ApiPrefix string `mapstructure:"apiPrefix" json:"apiPrefix"`
}

func (sc ServerConfigurations) HostWithPort() string {
	if strings.TrimSpace(sc.Host) == "" {
		log.Fatalf("host part is missing in server configurations")
	}
	return fmt.Sprintf("%s:%d", sc.Host, sc.Port)
}

// DatabaseConfigurations
type DatabaseConfigurations struct {
	DBName   string `mapstructure:"dbName" json:"dbName"`
	User     string `mapstructure:"user" json:"user"`
	Password string `mapstructure:"password" json:"password"`
	Host     string `mapstructure:"host" json:"host"`
	Port     string `mapstructure:"port" json:"port"`
	FileName string `mapstructure:"filename" json:"fileName"`
}

type AWSConfiguration struct {
	Endpoint   string `mapstructure:"endpoint"`
	Region     string `mapstructure:"region"`
	SecretName string `mapstructure:"secretName"`
	Current    string `mapstructure:"current"`
	Profile    string `mapstructure:"profile"`
}

func (ac AWSConfiguration) IsNil() bool {
	return ac.Endpoint == "" || ac.Region == "" ||
		ac.SecretName == "" || ac.Current == "" ||
		ac.Profile == ""
}
