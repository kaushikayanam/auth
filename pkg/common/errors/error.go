package errors

type ErrorType struct {
	t string
}

var (
	ErrorTypeUnknown       = ErrorType{"unknown"}
	ErrorTypeAuthorization = ErrorType{"authorization"}
	ErrorTypeIncorrect     = ErrorType{"incorrect-input"}
)
